@unnchapentry{Copying}{10001}{Copying}{1}
@unnchapentry{Contributors to Gorm}{10002}{Contributors}{2}
@numchapentry{Installing Gorm}{1}{Installation}{3}
@numsubsecentry{Required software}{1.0.1}{}{3}
@numsubsecentry{Build and Install}{1.0.2}{}{3}
@numsubsecentry{Trouble}{1.0.3}{}{3}
@numchapentry{News}{2}{News}{4}
@numsecentry{Noteworthy changes in version @samp {1.2.24}}{2.1}{}{4}
@numsecentry{Noteworthy changes in version @samp {1.2.23}}{2.2}{}{4}
@numsecentry{Noteworthy changes in version @samp {1.2.22}}{2.3}{}{4}
@numsecentry{Noteworthy changes in version @samp {1.2.20}}{2.4}{}{4}
@numsecentry{Noteworthy changes in version @samp {1.2.18}}{2.5}{}{4}
@numsecentry{Noteworthy changes in version @samp {1.2.16}}{2.6}{}{4}
@numsecentry{Noteworthy changes in version @samp {1.2.12}}{2.7}{}{4}
@numsecentry{Noteworthy changes in version @samp {1.2.10}}{2.8}{}{5}
@numsecentry{Noteworthy changes in version @samp {1.2.8}}{2.9}{}{5}
@numsecentry{Noteworthy changes in version @samp {1.2.6}}{2.10}{}{5}
@numsecentry{Noteworthy changes in version @samp {1.2.4}}{2.11}{}{5}
@numsecentry{Noteworthy changes in version @samp {1.2.2}}{2.12}{}{6}
@numsecentry{Noteworthy changes in version @samp {1.2.1}}{2.13}{}{6}
@numsecentry{Noteworthy changes in version @samp {1.2.0}}{2.14}{}{6}
@numsecentry{Noteworthy changes in version @samp {1.1.0}}{2.15}{}{6}
@numsecentry{Noteworthy changes in version @samp {1.0.8}}{2.16}{}{6}
@numsecentry{Noteworthy changes in version @samp {1.0.6}}{2.17}{}{7}
@numsecentry{Noteworthy changes in version @samp {1.0.4}}{2.18}{}{7}
@numsecentry{Noteworthy changes in version @samp {1.0.2}}{2.19}{}{7}
@numsecentry{Noteworthy changes in version @samp {1.0.0}}{2.20}{}{7}
@numsecentry{Noteworthy changes in version @samp {0.11.0}}{2.21}{}{7}
@numsecentry{Noteworthy changes in version @samp {0.9.10}}{2.22}{}{8}
@numsecentry{Noteworthy changes in version @samp {0.9.2}}{2.23}{}{8}
@numsecentry{Noteworthy changes in version @samp {0.9.0}}{2.24}{}{9}
@numsecentry{Noteworthy changes in version @samp {0.8.0}}{2.25}{}{9}
@numsecentry{Noteworthy changes in version @samp {0.7.7}}{2.26}{}{10}
@numsecentry{Noteworthy changes in version @samp {0.7.6}}{2.27}{}{10}
@numsecentry{Noteworthy changes in version @samp {0.7.5}}{2.28}{}{10}
@numsecentry{Noteworthy changes in version @samp {0.6.0}}{2.29}{}{11}
@numsecentry{Noteworthy changes in version @samp {0.5.0}}{2.30}{}{11}
@numsecentry{Noteworthy changes in version @samp {0.4.0}}{2.31}{}{11}
@numsecentry{Noteworthy changes in version @samp {0.3.1}}{2.32}{}{11}
@numsecentry{Noteworthy changes in version @samp {0.3.0}}{2.33}{}{11}
@numsecentry{Noteworthy changes in version @samp {0.2.5}.}{2.34}{}{12}
@numsecentry{Noteworthy changes in version @samp {0.2.0} snapshot.}{2.35}{}{12}
@numsecentry{Noteworthy changes in version @samp {0.1.0}}{2.36}{}{12}
@numsecentry{Noteworthy changes in version @samp {0.0.3}}{2.37}{}{13}
@numsecentry{Noteworthy changes in version @samp {0.0.2}}{2.38}{}{13}
@numsecentry{Noteworthy changes in version @samp {0.0.1}}{2.39}{}{13}
@numsubsecentry{To Do}{2.39.1}{}{14}
@numchapentry{Overview}{3}{Overview}{15}
@numsecentry{What You Must Know To Understand This Manual}{3.1}{}{15}
@numsubsecentry{Major features}{3.1.1}{}{15}
@numsecentry{About this Manual}{3.2}{}{15}
@numchapentry{Usage}{4}{Usage}{16}
@numchapentry{Implementation}{5}{Implementation}{20}
@numsecentry{Notes on implementation}{5.1}{}{20}
@numsecentry{Connections}{5.2}{}{20}
@numchapentry{Preferences}{6}{Preferences}{21}
@numchapentry{Basic Concepts}{7}{}{22}
@numsecentry{Getting Started}{7.1}{}{22}
@numsecentry{What is NSOwner?}{7.2}{}{22}
@numsecentry{What is NSFirst?}{7.3}{}{22}
@numsubsecentry{Responders}{7.3.1}{}{22}
@numsubsecentry{The Responder Chain}{7.3.2}{}{22}
@numsecentry{What is NSFont?}{7.4}{}{23}
@numsecentry{The awakeFromNib method}{7.5}{}{23}
@numchapentry{Creating an Application}{8}{}{24}
@numsecentry{Creating A Class In Gorm}{8.1}{}{24}
@numsecentry{Using The Outline View}{8.2}{}{24}
@numsubsecentry{Adding Outlets In The Outline View}{8.2.1}{}{24}
@numsubsecentry{Adding Actions In the Outline View}{8.2.2}{}{25}
@numsecentry{Using The Class Edit Inspector}{8.3}{}{25}
@numsubsecentry{Adding Outlets In The Inspector}{8.3.1}{}{25}
@numsubsecentry{Adding Actions In the Inspector}{8.3.2}{}{25}
@numsecentry{Instantiating The Class}{8.4}{}{25}
@numsecentry{Adding Controls from the Palette}{8.5}{}{25}
@numsubsecentry{Making Connections}{8.5.1}{}{25}
@numsecentry{Saving the gorm file}{8.6}{}{26}
@numsecentry{Generating .h and .m files from the class.}{8.7}{}{26}
@numchapentry{Another Simple Application}{9}{}{28}
@numsecentry{Adding Menu Items}{9.1}{}{28}
@numsecentry{Making a Controller-based .gorm file}{9.2}{}{28}
@numsubsecentry{Add the init method to WinController}{9.2.1}{}{29}
@numsecentry{Running the App}{9.3}{}{29}
@numchapentry{Advanced Topics}{10}{}{30}
@numsecentry{Gorm file format}{10.1}{}{30}
@numsubsecentry{The Name Table}{10.1.1}{}{30}
@numsubsecentry{The Custom Class Table}{10.1.2}{}{30}
@numsubsecentry{Connections Array}{10.1.3}{}{30}
@numsecentry{Custom Class Encoding}{10.2}{}{30}
@numsubsecentry{Restrictions On Your Custom Subclasses}{10.2.1}{}{31}
@numsecentry{Palettes}{10.3}{}{31}
@numsubsecentry{Graphical Objects In A Palette}{10.3.1}{}{31}
@numsubsecentry{Non Graphical Objects In A Palette}{10.3.2}{}{32}
@numchapentry{Frequently Asked Questions}{11}{}{33}
@numsubsecentry{Should I modify the data.classes of file in the .gorm package?}{11.0.1}{}{33}
@numsubsecentry{Why does my application crash when I add additional attributes for encoding in encodeWithCoder: or initWithCoder: in my custom class?}{11.0.2}{}{33}
@numsubsecentry{Why does Gorm give me a warning when I have bundles specified in GSAppKitUserBundles?}{11.0.3}{}{33}
@numsubsecentry{How can I avoid loading GSAppKitUserBundles in Gorm?}{11.0.4}{}{33}
@numsubsecentry{How can I change the font for a widget?}{11.0.5}{}{33}
@unnchapentry{Concept Index}{10003}{Concept Index}{35}
