\entry{features}{15}{features}
\entry{preferences}{21}{preferences}
\entry{defaults}{21}{defaults}
\entry{NSOwner}{22}{NSOwner}
\entry{NSFirst}{22}{NSFirst}
\entry{NSFont}{22}{NSFont}
\entry{NSResponder}{22}{NSResponder}
\entry{Responder Chain}{22}{Responder Chain}
\entry{Creating Classes}{24}{Creating Classes}
\entry{Classes Outline View}{24}{Classes Outline View}
\entry{Class Edit Inspector}{25}{Class Edit Inspector}
\entry{Instantiating}{25}{Instantiating}
\entry{Connections}{25}{Connections}
\entry{Saving}{26}{Saving}
\entry{Setting the NSOwner}{28}{Setting the NSOwner}
\entry{Connecting to a Window}{28}{Connecting to a Window}
\entry{Name Table}{30}{Name Table}
\entry{Custom Class Encoding}{30}{Custom Class Encoding}
\entry{Palettes}{31}{Palettes}
\entry{Inspectors}{31}{Inspectors}
\entry{Editors}{31}{Editors}
\entry{FAQ}{33}{FAQ}
